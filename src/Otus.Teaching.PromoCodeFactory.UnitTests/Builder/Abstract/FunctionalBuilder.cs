using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Abstractions
{

    public abstract class FunctionalBuilder<TSubject, TSelf>
        where TSelf : FunctionalBuilder<TSubject, TSelf>
        where TSubject : new()
    {
        private readonly List<Func<TSubject,TSubject>> actions
            = new List<Func<TSubject,TSubject>>();

        protected TSelf Do(Action<TSubject> action)
        => AddAction(action);

        public TSubject Build()
            => actions.Aggregate(new TSubject(), (x,y) => y(x));
        private TSelf AddAction(Action<TSubject> action)
        {
            actions.Add(x => {action(x);
                return x;
            });
            return (TSelf) this;
        }
    }
}