
using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builder
{
    public class PartnerBuilder : FunctionalBuilder<Partner,PartnerBuilder>
    {
        public PartnerBuilder()
        {
            Do(p => p.PartnerLimits = new List<PartnerPromoCodeLimit>());
        }
        public PartnerBuilder WithId(Guid id)
            => Do(p => p.Id = id);
        public PartnerBuilder WithName(string name)
            => Do(p => p.Name = name);
        public PartnerBuilder IsActive()
            => Do(p => p.IsActive = true);
        public PartnerBuilder IsInactive()
            => Do(p => p.IsActive = false);
        public PartnerBuilder HasNumberIssuedPromoCodes(int number)
            => Do(p => p.NumberIssuedPromoCodes = number);
        public PartnerBuilder HasLimit(PartnerPromoCodeLimit limit)
            => Do(p => p.PartnerLimits.Add(limit));
    }
}