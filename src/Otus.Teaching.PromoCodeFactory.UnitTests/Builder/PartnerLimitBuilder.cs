using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builder
{
    public class PartnerLimitBuilder : FunctionalBuilder<PartnerPromoCodeLimit, PartnerLimitBuilder>
    {
        public PartnerLimitBuilder WithId(Guid id)
            => Do(p => p.Id = id);
        public PartnerLimitBuilder CreatedAt (DateTime datetime)
            => Do(p => p.CreateDate = datetime);
            
        public PartnerLimitBuilder EndsAt (DateTime datetime)
            => Do(p => p.EndDate = datetime);
        
        public PartnerLimitBuilder CanceledAt (DateTime datetime)
            => Do(p => p.CancelDate = datetime);
            
        public PartnerLimitBuilder IsNotCanceled (DateTime datetime)
            => Do(p => p.CancelDate = null);
            
        public PartnerLimitBuilder WithLimit (int limit)
            => Do(p => p.Limit = limit);
    }
}