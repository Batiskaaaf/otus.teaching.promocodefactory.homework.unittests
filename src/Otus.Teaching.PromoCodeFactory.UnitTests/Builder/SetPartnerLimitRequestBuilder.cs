using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builder.Abstractions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builder
{
    public class SetPartnerLimitRequestBuilder : FunctionalBuilder<SetPartnerPromoCodeLimitRequest, SetPartnerLimitRequestBuilder>
    {
        public SetPartnerLimitRequestBuilder EndsAt (DateTime datetime)
            => Do(p => p.EndDate = datetime);
        
        public SetPartnerLimitRequestBuilder WithLimit (int limit)
            => Do(p => p.Limit = limit);
    }
}