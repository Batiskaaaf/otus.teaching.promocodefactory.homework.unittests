﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builder;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> partnersRepositoryMock;
        private readonly PartnersController partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
        {
            //Arrange
            var id = Guid.NewGuid();
            Partner partner = null;
            var request = new SetPartnerLimitRequestBuilder().Build();

            partnersRepositoryMock.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            //Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsLocked_ReturnsBadRequest()
        {
            var id = Guid.NewGuid();
            var partner = new PartnerBuilder().IsInactive().Build();
            var request = new SetPartnerLimitRequestBuilder().Build();
            partnersRepositoryMock.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsLessOrEqualToZero_ReturnsBadRequest()
        {
            var id = Guid.NewGuid();
            var partner = new PartnerBuilder().IsInactive().Build();
            var request = new SetPartnerLimitRequestBuilder().WithLimit(0).Build();
            partnersRepositoryMock.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitIsNotCanceled_NullifyIssuedPromocodesNumber()
        {
            var id = Guid.NewGuid();
            var partner = new PartnerBuilder()
                .HasNumberIssuedPromoCodes(10).IsActive()
                .HasLimit(new PartnerLimitBuilder().EndsAt(DateTime.Now.AddDays(5)).Build())
                .Build();
            var request = new SetPartnerLimitRequestBuilder().WithLimit(50).Build();
            partnersRepositoryMock.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitIsNotCanceled_CancelPreviousLimit()
        {
            var id = Guid.NewGuid();
            var limitId = Guid.NewGuid();
            var partner = new PartnerBuilder()
                .HasNumberIssuedPromoCodes(10).IsActive()
                .HasLimit(new PartnerLimitBuilder().EndsAt(DateTime.Now.AddDays(5)).WithId(limitId).Build())
                .Build();
            var request = new SetPartnerLimitRequestBuilder().WithLimit(50).Build();
            partnersRepositoryMock.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            partner.PartnerLimits.First(l => l.Id == limitId).CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitIsEnded_DoesntChangeIssuedPromocodesNumber()
        {
            var id = Guid.NewGuid();
            var limitId = Guid.NewGuid();
            var issuedPromoCodes = 10;
            var partner = new PartnerBuilder()
                .HasNumberIssuedPromoCodes(issuedPromoCodes).IsActive()
                .HasLimit(new PartnerLimitBuilder().EndsAt(DateTime.Now.AddDays(-5)).WithId(limitId).Build())
                .Build();
            var request = new SetPartnerLimitRequestBuilder().WithLimit(50).Build();
            partnersRepositoryMock.Setup(r => r.GetByIdAsync(id)).ReturnsAsync(partner);

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            partner.NumberIssuedPromoCodes.Should().Be(issuedPromoCodes);
        }
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenApplies_SavesNewLimit()
        {
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder().WithId(partnerId).IsActive().Build();
            partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var request = new SetPartnerLimitRequestBuilder().WithLimit(50).Build();

            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            partnersRepositoryMock.Verify(r => r.UpdateAsync(It.Is<Partner>(p => p.Id == partnerId && p.PartnerLimits.Any(l => l.Limit == 50))));
        }
    }
}